from bs4 import BeautifulSoup  # Библиотека для поиска тегов в html коде
import json  # Библиотека для работы с json
from urllib.parse import unquote
from database import DB  # Класс БД
import time  # Модуль для работы со временем
import config  # Модуль с различными константами
import queries  # Модуль с запросами к БД
import regions  # Модуль с регионами, в которых собирается информация
import logging  # Модуль для логгирования
import asyncio  # Модуль для асинхронности
import aiohttp  # Модуль для асинхронных запросов
from collections import namedtuple  # Именованный кортеж

logging.basicConfig(
	format='%(asctime)s %(levelname)-8s %(name)s - %(message)s',
	level=logging.DEBUG,
	datefmt='%Y-%m-%d %H:%M:%S'
)  # Настройки логгирования

logger = logging.getLogger('zoon_parser')  # Получение логгера


def get_json_from_string(string: str) -> list:
	""" функция для генерации данных в формате json из строки

	 :param: string: Строка, из которой будет сгенерировани json
	 :return: list: Список json-данных
	 """

	unquoted = unquote(string)  # Замена %XX последовательностей их строковыми эквивалентами
	json_data = json.loads(unquoted)  # Получение данных в формате json
	return json_data


def save_data_in_db(db: DB, data: list):
	""" Функция для сохранения данных в БД

	 :param db: Объект БД
	 :param data: Список коретежей данных
	 """

	query = queries.insert_or_update_records
	db.query_execute(query=query, params=data, ext=True)


def parse_json(json_data_list: list) -> dict:
	""" Функция для парсинга информации в формате json

	 :param json_data_list: Список информации в форате json
	 :param type_info: Тип информации в json
	 """

	obj_dict = dict()  # Словарь объектов

	for obj in json_data_list:
		id = obj.get('id')  # Идентификатор объекта

		obj_type = obj.get('type')  # Тип объекта (Например: ресторан, магазин, автосервис и т.д.)
		premium = 'Да' if obj.get('premium') else 'Нет'
		extra_info = obj.get('extra')  # Дополнительная информация об объекте
		service = extra_info.get('service', 'Не указана')  # Дополнительные сервисы
		cost = extra_info.get('cost', 'Не указана')  # Цена услуг

		Info = namedtuple('Info', 'type premium service cost')  # Создание именованного кортежа
		obj_dict[id] = Info(obj_type, premium, service,
							cost)  # Запись дополнительных параметров объекта по его идентификатору

	return obj_dict


async def parse_exstra(db: DB = None, link: str = None, data: str = None, points: str = None, blocks: list = None,
					   page: int = 1):
	""" Функция для парсинга дополнительной информации об объекте

	 :param db: Объект БД
	 :param link: Ссылка на ресурс
	 :param data: Данные, посылаемые в теле запроса
	 :param points: json-данные с данными об объектах
	 :param blocks: Блоки <li> с информацией об объектах
	 :param page: Номер страницы, на которую посылается запрос
	 """

	if page == 1:
		build_obj_list_and_save(db=db, points=points, blocks=blocks)
	else:
		async with aiohttp.ClientSession() as session:
			try:
				data = data[:-1] + str(page)  # Подстановка нужного номера страницы в data
				response = await session.post(url=link, params=config.PARAMS,
											  headers=config.HEADERS, data=data)  # Отправка асинхронного запроса
			except Exception as e:
				logger.error(f'Can\'t parse extra for {data}. Exception occurred: {e}')
				return
			else:
				# Проверка статус-кода ответа сервера
				status_code = response.status

				if status_code == 200:
					response_json = await response.json(encoding='UTF-8',
														content_type=None)  # Получение json-данных из ответа
					soup = BeautifulSoup(response_json['html'], 'html.parser')  # создание объекта BeautifulSoup

					blocks = soup.find('ul', class_='service-items-medium service-items-medium-hovered '
													'list-reset z-marker-events js-results-container'). \
						find_all('li', class_='minicard-item js-results-item')

					build_obj_list_and_save(db=db, points=points, blocks=blocks)
				else:
					logger.error(f'Can\'t parse extra for {data}')
					return


def build_obj_list_and_save(db: DB, points: str, blocks: list):
	""" Функция, формирующая список объектов и сохраняющая его в БД

	  :param db: Объект БД
	  :param points: Строка с информацией об объектах
	  :param blocks: Блоки <li> с информацией об объектах
	 """

	json_data = get_json_from_string(points)
	obj_dct = parse_json(json_data_list=json_data)
	obj_list = get_obj_info_from_html(blocks=blocks, obj_dct=obj_dct)
	save_data_in_db(db=db, data=obj_list)


def get_obj_info_from_html(blocks: list, obj_dct: dict) -> list:
	""" Функция для парсинга информации об объектах их html-кода

	 :param blocks: Список блоков <li> с информацией об объекте
	 :param obj_dct: Словарь с дополнительной информацией об объекте
	 """

	obj_list = []  # Список объектов

	for block in blocks:
		obj_id = block.get('data-id')  # Уникальный идентификатор объекта

		lat, lon = block.get('data-lat'), block.get('data-lon')  # Широта и долгота объекта

		# Блок с дополнительной информацией об объекте
		div_with_info = block.find('div', class_='minicard-item__container').find('div',
																				  class_='minicard-item__info')
		name = div_with_info.find('h2', class_='minicard-item__title')  # Название объекта
		if name: name = name.text.strip()  # Убрать пробелы до и после названия объекта

		rating = div_with_info.find('div', class_='minicard-item__rating').find('div', class_='stars'). \
			find('div', class_='stars-view stars-view-medium').get('title')  # Рейтинг объекта

		address_info = div_with_info.find('address', class_='minicard-item__address')  # Информация об адресе объекта
		address = address_info.find('span', class_='address')
		if address: address = address.text  # Адрес объекта

		metro = div_with_info.find('a', class_='metro')  # Ближайшее метро
		distance_info = None  # Время ходьбы до метро (мин.)

		if metro:
			metro = metro.text
			distance_info = address_info.find('span', class_='distance').text.strip()

		# Телефон объекта
		phone = div_with_info.find('div', class_='minicard-item__phone gray-phone')

		if phone:
			phone = phone.find('span', 'js-phone phone-hidden').find('a').get('href')
			phone = phone.split(':')[1]

		json_obj_info = obj_dct.get(obj_id)  # Получение дополнительной информации об объекте по его идентификатору

		obj_list.append(
			(obj_id, json_obj_info.type, name, address, metro, distance_info,
			 lat, lon, json_obj_info.premium, json_obj_info.service, rating,
			 json_obj_info.cost, phone)
		)

	return obj_list


async def parse_region(db: DB, link: str, coords: tuple):
	""" Функция для парсинга объектов местности с конкретными координатами

	 :param db: Объект БД
	 :param link: Ссылка на сайт для данного региона
	 :param: coords: Кортеж с координатами местности
	 """

	# Данные, посылаемые в теле запроса
	data = f'need%5B%5D=items&need%5B%5D=points&search_query_form=1&sort_field=photodistance&bounds%5B%5D={coords[0]}' \
		   f'&bounds%5B%5D={coords[1]}&bounds%5B%5D={coords[2]}&bounds%5B%5D={coords[3]}&page=1'

	# Отправка асинхронного запроса
	async with aiohttp.ClientSession() as session:
		response = await session.post(url=link, params=config.PARAMS, headers=config.HEADERS, data=data)

		if response.status == 200:
			response_json = await response.json(encoding='UTF-8', content_type=None)  # Получить json
			soup = BeautifulSoup(response_json['html'], 'html.parser')  # Получить объект BeautifulSoup

			points = soup.find('script', class_='points').text  # Получить скрипт с данными об объектах

			blocks = soup.find('ul', class_='service-items-medium service-items-medium-hovered '
											'list-reset z-marker-events js-results-container'). \
				find_all('li', class_='minicard-item js-results-item')  # Получить блоки с информацией об объектах

			await parse_exstra(db=db, points=points, blocks=blocks)

			for page in range(2, 11):
				await parse_exstra(db=db, link=link, data=data, points=points, page=page)
				await asyncio.sleep(60)
		else:
			logger.error(f'Can\'t parse coords {coords}')


async def common(db: DB, link: str, coords: tuple, is_change_lat: bool, is_increment: bool):
	""" Функция для парсинга информации об объектах для различных координат.
		Данная функция запускает цикл на 10 итераций, в котором асинхронно вызывается
		функция parse_region для различных координат области. На каждой итерации
		цикла функция изменяет координаты на 0.1 (инкрементирует либо декрементирует)

	  :param db: Объект БД
	  :param link: Строка запроса
	  :param coords: Кортеж с координатами местности
	  :param is_change_lat: Булева переменная, показывающая что нужно менять (долготу или широту)
	  :param is_increment: Булева переменная, показывающая как менять координаты
	  (инкрементировать или декрементировать)
	 """

	new_coords = coords  # Создание копии с координатами
	lat1, lon1, lat2, lon2 = coords

	for i in range(10):
		# Асинхронный вызов функции для парсинга региона с соответствующими координатами
		await parse_region(db, link, new_coords)

		# Изменение широты
		if is_change_lat:
			if is_increment:  # Инкрементация широты
				lat1 += .1
				lat2 += .1
			else:  # Декрементация широты
				lat1 -= .1
				lat2 -= .1
		else:  # Изменение долготы
			if is_increment:  # Инкрементация долготы
				lon1 += .1
				lon2 += .1
			else:  # Декрементация долготы
				lon1 -= .1
				lon2 -= .1

		new_coords = (lat1, lon1, lat2, lon2)  # Изменённые координаты области

		await asyncio.sleep(60)  # Рандомная задержка между итерациями цикла


async def parse_all_points(db: DB, link: str, coords: tuple, extra_coords: tuple):
	""" Функция для парсинга всех объектов конкретного города

	 :param db: Объект БД
	 :param link: Ссылка на сайт для конкретного города
	 :param coords: Список кортежей со стартовыми координатами для парсинга объектов
	 :param extra_coords: Список кортежей с дополнительными координатами
	 """

	tasks = []  # Список задач

	if coords:
		# Создание задач
		task1 = asyncio.create_task(common(db=db, link=link, coords=coords, is_change_lat=True, is_increment=True))
		task2 = asyncio.create_task(common(db=db, link=link, coords=coords, is_change_lat=True, is_increment=False))
		task3 = asyncio.create_task(common(db=db, link=link, coords=coords, is_change_lat=False, is_increment=True))
		task4 = asyncio.create_task(common(db=db, link=link, coords=coords, is_change_lat=False, is_increment=False))

		# Добавление задач в список
		tasks.append(task1)
		tasks.append(task2)
		tasks.append(task3)
		tasks.append(task4)

	if extra_coords:  # Если список кортежей с дополнительными координатами не пустой
		for extra_crd in extra_coords:
			task = asyncio.create_task(parse_region(db, link, extra_crd))  # Создание задачи
			tasks.append(task)  # Добавление задачи в список

	await asyncio.gather(*tasks)  # Одновременный запуск задач из списка


async def parse_city(db: DB, city_name: str):
	""" Функция для парсинга объектов из конкретного города

	 :param: db: Объект БД
	 :param: city_name: Название города
	 """

	tasks = []  # Список задач

	if city_name == 'spb':
		dict_link_coords = regions.dict_link_coords_spb
	elif city_name == 'msk':
		dict_link_coords = regions.dict_link_coords_msk
	else:
		dict_link_coords = regions.dict_link_coords_others

	for link, coords in dict_link_coords.items():
		# Создание задачи
		task = asyncio.create_task(parse_all_points(db=db, link=link,
													coords=coords[0], extra_coords=coords[1]))
		tasks.append(task)  # Добавление задачи в список

	await asyncio.gather(*tasks)  # Одновременный запуск задач из списка

	logger.info(f'Finish parsing {city_name}')


async def main():
	""" Точка входа """

	db = DB(
		# host='127.0.0.1',
		host=config.HOST,
		# port=5432,
		port=config.DB_PORT,
		# user='postgres',
		user=config.USERNAME,
		# password='postgreSQL123',
		password=config.PASSWORD,
		# db_name='zoon_db'
		db_name=config.DB_NAME
	)

	db.connection_with_db()

	logger.info(f'Создание таблицы {config.TABLE_NAME}')
	db.query_execute(query=queries.create_zoon_table)

	logger.info('Парсинг объектов из Санкт-Петербурга')
	await parse_city(db=db, city_name='spb')
	logger.info('Парсинг объектов из Санкт-Петербурга окончен')

	time.sleep(60)  # Задержка в одну минуту

	logger.info('Парсинг объектов из Москвы')
	await parse_city(db=db, city_name='msk')
	logger.info('Парсинг объектов из Москвы окончен')

	time.sleep(60)  # Задержка в одну минуту

	logger.info('Парсинг объектов из других городов')
	await parse_city(db=db, city_name='others')
	logger.info('Парсинг объектов из других городов окончен')

	records = db.query_execute(f""" SELECT COUNT(id) FROM {config.TABLE_NAME}; """, fetchone=True)[0]

	logger.info(f'В таблице {config.TABLE_NAME} {records} записей')

	db.connection_close()


if __name__ == '__main__':
	start = time.time()
	asyncio.run(main())
	finish = time.time()
	logger.info(f'Скрипт работал {round(finish - start, 2)} секунд')
