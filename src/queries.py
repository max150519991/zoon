import config

# Запрос на создание таблицы БД
create_zoon_table = f"""CREATE TABLE IF NOT EXISTS {config.TABLE_NAME}(
                                                        id VARCHAR(30) PRIMARY KEY, type VARCHAR(50), name VARCHAR(255), 
                                                        address VARCHAR(255), metro VARCHAR(100), walk_time_to_metro VARCHAR(20), 
                                                        lat VARCHAR(50), lon VARCHAR(50), premium VARCHAR(3), service VARCHAR(50), 
                                                        rating VARCHAR(100), cost VARCHAR(10), phone VARCHAR(30)
                                                        ); """

# Запрос на добавление или обновление записей в таблице
insert_or_update_records = f""" INSERT INTO {config.TABLE_NAME}(id, type, name, address, metro, walk_time_to_metro, 
						 lat, lon, premium, service, rating, cost, phone) 
                         VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT(id) DO UPDATE SET type=EXCLUDED.type, 
                         name=EXCLUDED.name, address=EXCLUDED.address, metro=EXCLUDED.metro, 
                         walk_time_to_metro=EXCLUDED.walk_time_to_metro, lat=EXCLUDED.lat, lon=EXCLUDED.lon, premium=EXCLUDED.premium, 
                         service=EXCLUDED.service, rating=EXCLUDED.rating, cost=EXCLUDED.cost, phone=EXCLUDED.phone; """