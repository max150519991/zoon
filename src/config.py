from os import getenv

# Константы для подключения к БД
HOST = getenv('POSTGRES_HOST', 'postgres') # Хост БД
USERNAME = getenv('POSTGRES_USER', 'starlinemaps') # Пользователь БД
PASSWORD = getenv('POSTGRES_PASSWORD', 'starlinemaps') # Пароль БД
DB_NAME = getenv('POSTGRES_DB', 'ZOON_DB') # Название БД
DB_PORT = getenv('POSTGRES_PORT', 5432) # Порт БД
TABLE_NAME = 'zoon' # Название таблицы БД

# Константы, необхнодимые для работы скрипта
HEADERS = {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/79.0.3945.88 Safari/537.36',
    'Accept': '*/*',
    'sec-ch-ua-platform': 'Windows',
    'X-Requested-With': 'XMLHttpRequest',
    'cookie': 'locale=ru_RU; anon_id=20230303182957mqlc.8b00; _ym_uid=1677857399323365655; _ym_d=1677857399; '
              '_ga=GA1.2.1741206951.1677857399; AATestGlobal=variation; DesktopCatalogOpenphoneButton=variation; '
              '_gid=GA1.2.1290358532.1679654976; city=msk; _ym_isad=2; sid=7ebe81c26424268581961018120137; '
              'auth_token=035ea5bd19f10056c3bb085718a6a5a4; _gat=1'

} # Заголовки в теле запроса

PARAMS = {
    'action': 'listJson',
    'type': 'service',
    'show_prices_on_map': '1',
} # Параметры запроса