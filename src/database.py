import psycopg2  # Библиотека для работы с PostgreSQL
from psycopg2 import extras
from time import sleep
import logging

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(name)s - %(message)s',
    level=logging.DEBUG,
    datefmt='%Y-%m-%d %H:%M:%S'
)
logger = logging.getLogger('DB')


class DB:
    __slots__ = ('__host', '__port', '__user', '__password', '__db_name',
                 '__connection', '__timeout')  # Кортеж атрибутов объекта

    def __init__(self, host, port, user, password, db_name):
        """ Конструктор """

        self.__host = host  # Хост для соединения с БД
        self.__port = port # Порт для соединения с БД
        self.__user = user  # Имя пользователя
        self.__password = password  # Пароль
        self.__db_name = db_name  # Название БД
        self.__connection = None  # Соединение
        self.__timeout = 60  # Тайм-аут подключения к БД

    def connection_with_db(self):
        """ Метод для установления соединения с БД """

        timeout = self.__timeout

        while timeout != 0:
            try:
                self.__connection = psycopg2.connect(
                    host=self.__host,
                    port=int(self.__port),
                    user=self.__user,
                    password=self.__password,
                    database=self.__db_name
                )
            except Exception as ex:
                logger.error(f'[ERROR] Failed connect to database {self.__db_name}: {ex}')
                timeout -= 1
                logger.info(f'[INFO] Tries left: {timeout}')
                sleep(1)
            else:
                logger.debug('[DEBUG] Database connection established')
                self.__connection.autocommit = True
                break

        else:
            raise Exception(f'Failed connect to database {self.__db_name}')

    def query_execute(self, query, params=(), fetchone=False, fetchall=False, ext=False):
        """
        Метод, выполняющий запрос к БД

        :param query: запрос к БД
        """

        if not self.__connection:
            self.connection_with_db()

        with self.__connection.cursor() as cur:
            try:
                if ext:
                    extras.execute_batch(cur, query, params)
                else:
                    cur.execute(query, params)

                if fetchone:
                    res = cur.fetchone()
                    return res

                elif fetchall:
                    res = cur.fetchall()
                    return res

            except Exception as e:
                logger.error(f'[ERROR] An exception occurred: {e}')
                self.connection_close()

    def connection_close(self):
        """ Метод, закрывающий соединение с БД """

        if self.__connection:
            self.__connection.close()
            logger.debug('[DEBUG] Database connection closed')
